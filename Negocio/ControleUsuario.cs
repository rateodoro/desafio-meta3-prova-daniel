﻿using Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    public static class ControleUsuario
    {
        public static Usuario ObterUsuarioPorLogin(string login)
        {
            return Repositorio.UsuarioRepositorio.ObterUsuarioPorLogin(login);
        }

        public static bool ValidaLogin(string login, string senha)
        {
            return Repositorio.UsuarioRepositorio.ValidaLogin(login, senha);
        }
    }
}
