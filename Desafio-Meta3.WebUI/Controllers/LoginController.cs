﻿using Desafio_Meta3.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Desafio_Meta3.WebUI.Controllers
{
    [Authorize]
    public class LoginController : Controller
    {
        [AllowAnonymous]
        public ActionResult Autenticacao()
        {
            HttpCookie authCookie = HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie == null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home", new { Area = "" });
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Autenticacao(LoginViewModel vm)
        {
            if (!ModelState.IsValid)
                return View("Login");

            Session.Clear();
            
            var valido = Negocio.ControleUsuario.ValidaLogin(vm.Usuario.Login, vm.Usuario.Senha);
            
            if (valido)
            {
                vm.Usuario = Negocio.ControleUsuario.ObterUsuarioPorLogin(vm.Usuario.Login);
                #region Sessão do usuário

                Session["UsuarioLogado"] = true;
                Session["UsuarioAtivo"] = vm.Usuario.Ativo;
                Session["UsuarioCodigo"] = vm.Usuario.IdUsuario;
                Session["UsuarioNome"] = vm.Usuario.Login;
                Session["UsuarioNomeCompleto"] = vm.Usuario.Nome;
                Session["UsuarioCpf"] = vm.Usuario.Cpf;
                Session["UsuarioCodPessoaFisica"] = vm.Usuario.IdPessoaFisica;
                Session["UsuarioDataInclusao"] = vm.Usuario.DataInclusao;

                String dadosSessao = "UsuarioNome=" + vm.Usuario.Login + ","
                                   + "UsuarioCodPessoaFisica=" + vm.Usuario.IdPessoaFisica + ","
                                   + "UsuarioCodigo=" + vm.Usuario.IdUsuario;

                #endregion

                #region Cookie para gravar informações do login

                int timeout = vm.LembrarSenha ? 525600 : 15;

                FormsAuthentication.SetAuthCookie(vm.Usuario.Login, false);

                var authTicket = new FormsAuthenticationTicket(1, vm.Usuario.Login, DateTime.Now, DateTime.Now.AddMinutes(timeout), true, dadosSessao);
                string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);

                if (vm.LembrarSenha)
                    authCookie.Expires = DateTime.Now.AddYears(5);

                HttpContext.Response.Cookies.Add(authCookie);

                #endregion

                return RedirectToAction("Index", "Home");
            }

            ViewBag.Error = " Usuário e/ou senha inválido(s) ou inexistente(s).";

            return RedirectToAction("Autenticacao", "Login");
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Clear();
            return RedirectToAction("Autenticacao", "Login");
        }
    }
}